package progetto;

import java.util.stream.IntStream;

public class Fizzbuzz {


    public static String fizzbuzzer(int n){

        String result = "";

        for(int i=1; i<=n; i++)
        {
            if((i%3) == 0 && (i%5) == 0)
                result += "fizzbuzz ";
            
            else if((i%3) == 0)
                result += "fizz ";

            else if((i%5) == 0)
                result += "buzz ";

            else
                result += String.format("%d ",i);
        }

        return result;
    }

}
